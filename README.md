# Basket CLI application
## Requirements
* PHP7.1.X
* Symfony 4.1.X

## Installation
Step 1
```sh
git clone https://arthur_frank@bitbucket.org/arthur_frank/basket.git
```

Step 2

In folder you cloned the code base run:
```sh
composer install
```

NOTE: If you do not have composer installed on your computer see this:
https://getcomposer.org/doc/00-intro.md

Once you have got composer command working, get back to Step 2.

This should finish your installation.

## Usage
This is pure CLI application, so use terminal to navigate to the folder where you have installed the applciation.

To see all available Symfony commands run:
```sh
bin/console
```

To run basket total price calculation with provided SKU (Stock Keeping Unit) codes run:
```sh
bin/console app:basket:get-basket-total [A,B,C,D]
```

NOTE: Parameter SKUs is optional, example with provided SKUs will look like this:

```sh
bin/console app:basket:get-basket-total AAABB
```
SKUs which do not exists within the app, will be ignored!
SKUs param is not case sensitive, so this:
```sh
bin/console app:basket:get-basket-total aaAbB
```
Should work as well.

## Tests
For tests is used PHPUnit v7.3

To run all tests simply navigate to the project folder via terminal and run:
```sh
bin/phpunit
```

Ig you would like to filter tests and run specific tests then example will look lke this:
```sh
bin/phpunit --filter NameOfTheTestClassYouWantToRun
```
