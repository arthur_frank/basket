<?php

namespace AF\Basket\Domain\Basket;

use AF\Basket\Domain\Money\Currency;
use AF\Basket\Domain\Money\CurrencySymbol;
use AF\Basket\Domain\Money\Money;
use AF\Basket\Domain\Product\Product;
use AF\Basket\Domain\Product\ProductFactory;

/**
 * Basket
 *
 * Domain object to represent a Basket.
 */
class Basket
{
    /**
     * @var array Array of products
     */
    private $products;

    /**
     * Basket constructor.
     *
     * @param  $productFactory
     */
    public function __construct(ProductFactory $productFactory)
    {
        $this->productFactory = $productFactory;
    }

    /**
     * Mutator method.
     *
     * @param array $products
     *
     * @return Basket
     */
    public function setProducts(array $products): self
    {
        $this->products = array_map(function ($product) {
            return $this->productFactory->createFromApiResponse($product);
        }, $products);

        return $this;
    }

    /**
     * Accessor method.
     *
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @return Money
     */
    public function getTotalPrice(): Money
    {
        $totalPrice = 0;

        $groupedProducts = $this->getGroupedProducts();

        foreach ($groupedProducts as $productGroup) {
            /**
             * @var Product $product
             */
            $product = $productGroup[0];

            // If we are dealing with special offer product we need to apply special conditions to the price.
            if ($product->getSpecialOfferCondition() !== null) {
                // Total count of products in the group.
                $productsCount = count($productGroup);

                $totalProductGroupPrice = $this->calcSpecialOfferGroupPrice(
                    $product,
                    $productsCount
                );

                // Total price for all products in the basket so far.
                $totalPrice += $totalProductGroupPrice;
            } else {
                // If product is not having special offer just sum prices.
                $totalPrice += $product->getPrice()->getAmount();
            }
        }

        return new Money($totalPrice, new Currency(CurrencySymbol::GBP_ISO));
    }

    /**
     * @return array
     */
    public function getGroupedProducts(): array
    {
        $groupedProducts = [];

        /**
         * @var Product $product
         */
        foreach ($this->products as $key => $product) {
            $groupedProducts[$product->getSku()][] = $product;
            unset($this->products[$key]); // Release memory.
        }

        return $groupedProducts;
    }

    /**
     * @param Product $product
     * @param int $productsCount
     *
     * @return int
     */
    private function calcSpecialOfferGroupPrice(Product $product, int $productsCount): int
    {
        $offerCondition = $product->getSpecialOfferCondition();
        $productPrice = $product->getPrice()->getAmount();
        $productOfferPrice = $product->getSpecialOfferPrice()->getAmount();

        // Amount of offers. Ex. 3 apples for 130 is 1 offer
        $offers = floor($productsCount / $offerCondition);

        // Amount of products which didn't get under special offer condition.
        $noOfferProducts = ($productsCount - ($offers * $offerCondition));

        // Total price for products that landed under offer condition.
        $offerTotalPrice = $offers * $productOfferPrice;

        // Total price for product group including products under offer and those which didn't get into offer.
        $totalProductGroupPrice = $offerTotalPrice + $noOfferProducts * $productPrice;

        return $totalProductGroupPrice;
    }
}
