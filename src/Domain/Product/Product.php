<?php

namespace AF\Basket\Domain\Product;

use AF\Basket\Domain\Money\Money;

/**
 * Product
 *
 * Domain object to represent a Product.
 */
class Product
{
    /**
     * Product Name
     *
     * @var string
     */
    private $name;

    /**
     * SKU - Stock Keeping Unit
     *
     * @var string
     */
    private $sku;

    /**
     * @var Money
     */
    private $price;

    /**
     * @var Money
     */
    private $offerPrice;

    /**
     * @var int
     */
    private $offerCondition;

    /**
     * Product constructor.
     *
     * @param string $name
     * @param $sku
     * @param Money $price
     * @param null|Money $offerPrice
     * @param null|int $offerCondition
     */
    public function __construct(
        string $name,
        string $sku,
        Money $price,
        ?Money $offerPrice = null,
        ?int $offerCondition = null
    ) {
        $this->setName($name);
        $this->setPrice($price);
        $this->setSku($sku);
        $this->setSpecialOfferPrice($offerPrice);
        $this->setSpecialOfferCondition($offerCondition);
    }

    /**
     * Mutator method.
     *
     * @param string $name
     *
     * @return Product
     */
    private function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Accessor method.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Mutator method.
     *
     * @param Money $price
     *
     * @return Product
     */
    private function setPrice(Money $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Accessor method.
     *
     * @return Money
     */
    public function getPrice(): Money
    {
        return $this->price;
    }

    /**
     * Mutator method.
     *
     * @param string $sku
     *
     * @return Product
     */
    private function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Accessor method.
     *
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * Mutator method.
     *
     * @param null|Money $offerPrice
     *
     * @return Product
     */
    private function setSpecialOfferPrice(?Money $offerPrice): self
    {
        $this->offerPrice = $offerPrice;

        return $this;
    }

    /**
     * Accessor method.
     *
     * @return null|Money
     */
    public function getSpecialOfferPrice(): ?Money
    {
        return $this->offerPrice;
    }

    /**
     * Mutator method.
     *
     * @param null|int $offerCondition
     *
     * @return Product
     */
    private function setSpecialOfferCondition(?int $offerCondition): self
    {
        $this->offerCondition = $offerCondition;

        return $this;
    }

    /**
     * Accessor method.
     *
     * @return null|int
     */
    public function getSpecialOfferCondition(): ?int
    {
        return $this->offerCondition;
    }
}
