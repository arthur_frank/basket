<?php

namespace AF\Basket\Domain\Product;

use AF\Basket\Domain\Money\Money;
use AF\Basket\Infrastructure\Http\Client\ClientInterface;

/**
 * ProductRepositoryInterface
 *
 * A repository to interact with Product API.
 */
interface ProductRepositoryInterface
{
    /**
     * Retrieve products by SKU codes.
     *
     * @param array $skus
     *
     * @return array
     */
    public function getProductsBySkus(array $skus): array;
}
