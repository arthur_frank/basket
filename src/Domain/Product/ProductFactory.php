<?php

namespace AF\Basket\Domain\Product;

use AF\Basket\Domain\Money\Currency;
use AF\Basket\Domain\Money\Money;

/**
 * ProductFactory
 *
 * Factory to create product domain objects.
 */
class ProductFactory
{
    public function createFromApiResponse(array $apiProduct): Product
    {
        $offerPrice = null;
        $offerCondition = null;

        $price = new Money(
            $apiProduct['price'],
            new Currency($apiProduct['currency'])
        );

        if (!empty($apiProduct['special_offer']) &&
            isset($apiProduct['special_offer']['offer_price'])
        ) {
            $offerPrice = new Money(
                $apiProduct['special_offer']['offer_price'],
                new Currency($apiProduct['currency'])
            );

            $offerCondition = $apiProduct['special_offer']['condition_amount'];
        }

        return new Product(
            $apiProduct['name'],
            $apiProduct['sku'],
            $price,
            $offerPrice,
            $offerCondition
        );
    }
}
