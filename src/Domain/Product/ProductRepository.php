<?php

namespace AF\Basket\Domain\Product;

use AF\Basket\Infrastructure\Http\Client\ClientInterface;

/**
 * ProductRepository
 *
 * A repository to interact with Product API.
 */
class ProductRepository implements ProductRepositoryInterface
{
    /**
     * Http client
     *
     * @var ClientInterface
     */
    private $client;

    /**
     * @var string
     */
    private $apiUrl;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * Product constructor.
     *
     * @param ClientInterface $client
     * @param string $apiUrl
     * @param string $apiKey
     */
    public function __construct(ClientInterface $client, string $apiUrl, string $apiKey)
    {
        $this->client = $client;
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
    }

    /**
     * {@inheritdoc}
     */
    public function getProductsBySkus(array $skus): array
    {
        $products = $this->fixtureData();
        $mappedProducts = array_map(function ($sku) use ($products) {
            if (array_key_exists($sku, $products)) {
                return $products[$sku];
            }
            return null;
        }, $skus);

        return array_filter($mappedProducts, function ($product) {
                return $product !== null;
            }
        );
    }

    /**
     * I fixture data, to simulate response from API or results from DB.
     *
     * @return array
     */
    private function fixtureData(): array
    {
        /**
         * This is fixture data.
         * In real application that should be API call and data fetched from the endpoint.
         * Another option is to retrieve this data from DB (MySQL/MongoDB/Elasticsearch etc.)
         */
        return [
            'A' => [
                'sku' => 'A',
                'name' => 'Apple',
                'price' => 50,
                'currency' => 'GBP',
                'special_offer' => [
                    'condition_amount' => 3,
                    'offer_price' => 130
                ]
            ],
            'B' => [
                'sku' => 'B',
                'name' => 'Banana',
                'price' => 30,
                'currency' => 'GBP',
                'special_offer' => [
                    'condition_amount' => 2,
                    'offer_price' => 45
                ]
            ],
            'C' => [
                'sku' => 'C',
                'name' => 'Cucumber',
                'price' => 20,
                'currency' => 'GBP',
                'special_offer' => []
            ],
            'D' => [
                'sku' => 'D',
                'name' => 'Damson',
                'price' => 15,
                'currency' => 'GBP',
                'special_offer' => []
            ],
        ];
    }

}
