<?php

namespace AF\Basket\Domain\Money;

/**
 * Currency
 *
 * Domain object to represent a Currency.
 */
class Currency
{
    /**
     * @var string
     */
    private $isoCode;

    /**
     * Currency constructor.
     *
     * @param string $anIsoCode
     */
    public function __construct(string $anIsoCode)
    {
        $this->setIsoCode($anIsoCode);
    }

    /**
     * Mutator method.
     *
     * @param string $anIsoCode
     *
     * @return Currency
     */
    private function setIsoCode(string $anIsoCode): self
    {
        if (!preg_match('/^[A-Z]{3}$/', $anIsoCode)) {
            throw new \InvalidArgumentException('ISO code must be three uppercase letters.');
        }

        $this->isoCode = $anIsoCode;

        return $this;
    }

    /**
     * Accessor method.
     *
     * @return string
     */
    public function getIsoCode(): string
    {
        return $this->isoCode;
    }

    /**
     * @return string
     */
    public function getCurrencySymbol(): string
    {
        return CurrencySymbol::CURRENCY_SYMBOLS[$this->isoCode] ?? CurrencySymbol::N_A;
    }
}
