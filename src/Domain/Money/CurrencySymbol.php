<?php

namespace AF\Basket\Domain\Money;

/**
 * Static object to map currency to it's symbol.
 *
 * CurrencySymbol
 */
class CurrencySymbol
{
    /**
     * @var string Value for undefined currency.
     */
    const N_A = 'N/A';

    /**
     * @var string Euro symbol.
     */
    const EUR = '€';

    /**
     * @var string Dollar symbol.
     */
    const USD = '$';

    /**
     * @var string British Pound symbol.
     */
    const GBP = '£';

    /**
     * @var string Euro ISO code.
     */
    const EUR_ISO = 'EUR';

    /**
     * @var string Dollar ISO code.
     */
    const USD_ISO = 'USD';

    /**
     * @var string British Pound ISO code.
     */
    const GBP_ISO = 'GBP';

    /**
     * @var array An array of currencies.
     */
    const CURRENCY_SYMBOLS = [
        self::EUR_ISO => self::EUR,
        self::USD_ISO => self::USD,
        self::GBP_ISO => self::GBP,
    ];
}
