<?php

namespace AF\Basket\Domain\Money;

use InvalidArgumentException;

/**
 * MoneyFactory
 *
 * A Factory to create Money.
 */
class MoneyFactory
{
    /**
     * @param int $amount
     * @param Currency $currency
     *
     * @throws InvalidArgumentException
     *
     * @return Money
     */
    public function create(int $amount, Currency $currency): Money
    {
        return new Money($amount, $currency);
    }

    /**
     * @param float $floatAmount
     * @param Currency $currency
     *
     * @throws InvalidArgumentException
     *
     * @return Money
     */
    public function createFromFloatAmount(float $floatAmount, Currency $currency): Money
    {
        if (!preg_match('/^[0-9]+[\.]?[0-9]{0,2}$/', $floatAmount)) {
            throw new InvalidArgumentException(
                'Float amount must not have more then two digits after comma.'
            );
        }

        $amount = (int) ($floatAmount * 100);

        return new Money($amount, $currency);
    }
}
