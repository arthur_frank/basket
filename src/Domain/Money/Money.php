<?php

namespace AF\Basket\Domain\Money;

/**
 * Money
 *
 * Domain object to represent a Money.
 */
class Money
{
    /**
     * A value that makes 1 complete money unit.
     * Ex. 100 pence will make £1, and 100 cents will make $1 etc.
     *
     * @var int
     */
    const ONE_MONEY_UNIT_VALUE = 100;

    /**
     * Money amount in lowest nominal ex. [pence]
     *
     * @var int
     */
    private $amount;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * Money constructor.
     *
     * @param int $anAmount
     * @param Currency $aCurrency
     */
    public function __construct(int $anAmount, Currency $aCurrency)
    {
        $this->setAmount($anAmount);
        $this->setCurrency($aCurrency);
    }

    /**
     * Mutator method.
     *
     * @param int $anAmount
     *
     * @return Money
     */
    private function setAmount(int $anAmount): self
    {
        $this->amount = $anAmount;

        return $this;
    }

    /**
     * Accessor method.
     *
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * Mutator method.
     *
     * @param Currency $aCurrency
     *
     * @return Money
     */
    private function setCurrency(Currency $aCurrency): self
    {
        $this->currency = $aCurrency;

        return $this;
    }

    /**
     * Accessor method.
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }
}
