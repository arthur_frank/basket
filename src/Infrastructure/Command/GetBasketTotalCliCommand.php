<?php

namespace AF\Basket\Infrastructure\Command;

use AF\Basket\Domain\Basket\Basket;
use AF\Basket\Domain\Product\ProductRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetBasketTotalCliCommand extends Command
{
    /**
     * @var string
     */
    const CLI_COMMAND_NAME = 'app:basket:get-basket-total';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param Basket $basket
     */
    public function __construct(
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        Basket $basket
    ) {
        parent::__construct(self::CLI_COMMAND_NAME);
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->basket = $basket;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName(self::CLI_COMMAND_NAME)
            ->addArgument('skus', InputArgument::OPTIONAL, 'A string with list of SKU\'s. Ex. ABC')
            ->setDescription('Returns total price for list of items in the basket.')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $listOfSkuCodes = $input->getArgument('skus');

        if (empty($listOfSkuCodes)) {
            $output->writeln('<info>Total price: 0</info>');

            return;
        }

        $this->validateInput($listOfSkuCodes);

        $products = $this->productRepository->getProductsBySkus(
            str_split(trim(strtoupper($listOfSkuCodes)))
        );

        $this->basket->setProducts($products);
        $totalPrice = $this->basket->getTotalPrice();

        $output->writeln(
            '<info>Total price: ' . $totalPrice->getAmount() . '</info>'
        );
    }

    private function validateInput(string $skus): void
    {
        if (!is_string($skus)) {
            throw new \InvalidArgumentException('SKU list should be a valid string');
        }
    }

}
