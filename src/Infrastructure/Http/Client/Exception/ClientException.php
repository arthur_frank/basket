<?php

namespace AF\Basket\Infrastructure\Http\Client\Exception;

use GuzzleHttp\Exception\ClientException as GuzzleClientException;

/**
 * ClientException
 *
 * Exception class for client errors.
 */
class ClientException extends GuzzleClientException
{
}
