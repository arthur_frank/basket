<?php

namespace AF\Basket\Infrastructure\Http\Client;

use AF\Basket\Infrastructure\Http\Client\Exception\ClientException;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;

/**
 * GuzzleClientAdapter
 *
 * Class for a client for making HTTP requests via Guzzle.
 */
final class GuzzleClientAdapter implements ClientInterface
{
    /**
     * @var GuzzleClientInterface A Guzzle client.
     */
    private $client;

    /**
     * @var array Array of additional options common to all requests made by this object (unless overridden).
     */
    private $options;

    /**
     * @var LoggerInterface A logger.
     */
    private $logger;

    /**
     * @var array Static array of sensitive header names, i.e. headers which may contain usernames etc.
     */
    private static $sensitiveHeaderNames = ['Authorization'];

    /**
     * Constructor.
     *
     * @param GuzzleClientInterface $client A Guzzle client.
     * @param LoggerInterface $logger A logger.
     * @param array $options Array of additional options common to all requests made by this object (unless overridden).
     */
    public function __construct(
        GuzzleClientInterface $client,
        LoggerInterface $logger,
        array $options = []
    ) {
        $this->logger = $logger;
        $this->client = $client;
        $this->options = $options;
    }

    /**
     * {@inheritdoc}
     */
    public function request(
        string $method,
        string $uri,
        array $headers = [],
        $body = null,
        array $options = []
    ) {
        $guzzleOptions = array_merge(
            [
                'headers' => $headers,
                'body' => $body
            ],
            $this->getOptions(),
            $options
        );

        try {
            // Log http request attempt.
            $this->logger->info(
                'Making HTTP request',
                [
                    'url' => $uri,
                    'method' => $method,
                    'headers' => $guzzleOptions['headers'],
                    'body' => (string) $guzzleOptions['body'],
                    'options' => $options
                ]
            );

            return $this->client->request($method, $uri, $guzzleOptions);
        } catch (RequestException $e) {
            $request = $e->getRequest();

            // Filter out sensitive info from headers.
            $requestHeaders = $this->filterSensitiveHeaders($e->getRequest()->getHeaders());
            foreach ($requestHeaders as $name => $value) {
                $request = $request->withHeader($name, $value);
            }

            $errorMessage = $e->getMessage();

            $this->logger->error(
                $errorMessage,
                [
                    'request' => $request,
                    'response' => $e->getResponse()
                ]
            );

            throw new ClientException($errorMessage, $request);
        }
    }

    /**
     * Get the options for the client, ensuring boolean values are boolean.
     *
     * @return array An array of options.
     */
    private function getOptions()
    {
        $options = [];

        foreach ($this->options as $key => $option) {
            if ($option === '1' || $option === '' || $option === '0') {
                $options[$key] = (bool) $option;
            } else {
                $options[$key] = $option;
            }
        }

        return $options;
    }

    /**
     * Remove sensitive headers from logs and errors.
     *
     * @param array $headers An array of headers.
     *
     * @return array The input array of headers with sensitive information redacted.
     */
    private function filterSensitiveHeaders(array $headers) : array
    {
        foreach ($headers as $headerName => $headerValue) {
            if (in_array($headerName, self::$sensitiveHeaderNames, true)) {
                $headers[$headerName] = 'REDACTED';
            }
        }

        return $headers;
    }
}
