<?php

namespace AF\Basket\Infrastructure\Http\Client;

/**
 * ClientInterface
 *
 * Interface for a client to make HTTP(S) requests.
 */
interface ClientInterface
{
    /**
     * Make a request.
     *
     * @param string $method The HTTP verb, e.g. 'POST'.
     * @param string $uri The URL, including the querystring.
     * @param array $headers An associative array of headers.
     * @param string|null $body The request body.
     * @param array $options An array of options for the client (e.g. timeouts, certificate verification etc.).
     *
     * @return mixed
     */
    public function request(
        string $method,
        string $uri,
        array $headers = [],
        ?string $body = null,
        array $options = []
    );
}
