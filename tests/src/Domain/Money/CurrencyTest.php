<?php

namespace AF\Basket\Tests\Money;

use AF\Basket\Domain\Money\Currency;
use PHPUnit\Framework\TestCase;
use AF\Basket\Domain\Money\CurrencySymbol;

/**
 * CurrencyTest
 *
 * @group unit
 */
class CurrencyTest extends TestCase
{
    public function testCurrencyReturnExpectedIsoCode(): void
    {
        $currencyIsoCode = 'GBP';

        $currency = new Currency($currencyIsoCode);

        $this->assertSame($currencyIsoCode, $currency->getIsoCode());
    }

    public function testCurrencyThrowsExceptionIfIsoCodeIsInvalid(): void
    {
        $currencyIsoCode = 'POUND';

        $caught = false;

        try {
            new Currency($currencyIsoCode);
        } catch (\InvalidArgumentException $e) {
            $caught = true;
        }

        $this->assertTrue($caught);
    }

    /**
     * @param string $isoCode
     * @param string $expectedSymbol
     *
     * @dataProvider getCurrencyCodes
     */
    public function testCurrencyReturnsValidCurrencySymbols(string $isoCode, string $expectedSymbol): void
    {
        $currency = new Currency($isoCode);

        $this->assertSame($expectedSymbol, $currency->getCurrencySymbol());
    }

    /**
     * Data provider for currency codes.
     *
     * @return array
     */
    public function getCurrencyCodes(): array
    {
        return [
            [CurrencySymbol::EUR_ISO, CurrencySymbol::EUR],
            [CurrencySymbol::USD_ISO, CurrencySymbol::USD],
            [CurrencySymbol::GBP_ISO, CurrencySymbol::GBP],
            ['FOO', CurrencySymbol::N_A],
        ];
    }
}
