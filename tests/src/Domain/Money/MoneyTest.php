<?php

namespace AF\Basket\Tests\Money;

use AF\Basket\Domain\Money\Currency;
use AF\Basket\Domain\Money\Money;
use PHPUnit\Framework\TestCase;

/**
 * MoneyTest
 *
 * @group unit
 */
class MoneyTest extends TestCase
{
    public function testMoneyIsReturnsExpectedAmount(): void
    {
        $amount = 99;

        $money = new Money($amount, new Currency('GBP'));

        $this->assertTrue(is_int($money->getAmount()));
        $this->assertSame($amount, $money->getAmount());
    }

    public function testMoneyReturnsExpectedCurrency(): void
    {
        $money = new Money(0, new Currency('GBP'));

        $this->assertInstanceOf(Currency::class, $money->getCurrency());
    }

    public function testMoneyCreatesExpectedObjectInstance(): void
    {
        $money = new Money(99, new Currency('GBP'));

        $this->assertInstanceOf(Money::class, $money);
    }
}
