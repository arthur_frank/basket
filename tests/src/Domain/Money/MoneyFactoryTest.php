<?php

namespace AF\Basket\Tests\Money;

use AF\Basket\Domain\Money\Currency;
use AF\Basket\Domain\Money\Money;
use AF\Basket\Domain\Money\MoneyFactory;
use PHPUnit\Framework\TestCase;

/**
 * MoneyFactoryTest
 *
 * @group unit
 */
class MoneyFactoryTest extends TestCase
{
    public function testMoneyFactoryCreatesExpectedMoneyFromFloatAmount(): void
    {
        $floatAmount = 99.98;
        $expectedAmount = 9998;

        $moneyFactory = new MoneyFactory();
        $money = $moneyFactory->createFromFloatAmount(
            $floatAmount,
            new Currency('GBP')
        );

        $this->assertTrue(is_int($money->getAmount()));
        $this->assertSame($expectedAmount, $money->getAmount());
    }

    /**
     * @param float $amount Amount
     *
     * @dataProvider amountDataProvider
     */
    public function testMoneyFactoryCreatesExpectedMoneyObject(float $amount): void
    {

        $moneyFactory = new MoneyFactory();
        $money = $moneyFactory->createFromFloatAmount($amount, new Currency('GBP'));

        $this->assertInstanceOf(Money::class, $money);
    }

    public function testMoneyFactoryThrowsExceptionIfAmountIsInvalid(): void
    {
        $floatAmount = 99.9999;

        $moneyFactory = new MoneyFactory();

        $caught = false;

        try {
            $moneyFactory->createFromFloatAmount(
                $floatAmount,
                new Currency('GBP')
            );
        } catch (\InvalidArgumentException $e) {
            $caught = true;
        }

        $this->assertTrue($caught);
    }

    /**
     * Data provider for valid money amount.
     *
     * @return array A data provider array.
     */
    public function amountDataProvider(): array
    {
        return [
            [99],
            [99.],
            [99.0],
            [99.1],
            [99.01],
            [0.01],
            [0.0],
            [0],
        ];
    }
}
