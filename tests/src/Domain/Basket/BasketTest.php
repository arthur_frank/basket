<?php

namespace AF\Basket\Tests\Basket;

use AF\Basket\Domain\Basket\Basket;
use AF\Basket\Domain\Product\Product;
use AF\Basket\Domain\Product\ProductFactory;
use AF\Basket\Domain\Product\ProductRepository;
use AF\Basket\Domain\Product\ProductRepositoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * BasketTest
 *
 * @group unit
 */
class BasketTest extends TestCase
{
    /**
     * @var string Stock Keeping Unit for apple
     */
    const SKU_APPLE = 'A';

    /**
     * @var string Stock Keeping Unit for banana
     */
    const SKU_BANANA = 'B';

    /**
     * @var string Stock Keeping Unit for cucumber
     */
    const SKU_CUCUMBER = 'C';

    /**
     * @var string Stock Keeping Unit for Damson
     */
    const SKU_DAMSON = 'D';

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    public function setUp()
    {
        $this->basket = new Basket(new ProductFactory());
        $this->productRepository = $this->getMockBuilder(ProductRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testBasketSetsProductsAsExpected(): void
    {
        $this->basket->setProducts($this->getFixtureProducts());

        foreach ($this->basket->getProducts() as $product) {
            $this->assertInstanceOf(Product::class, $product);
        }
    }

    public function testBasketGroupsProductsAsExpected(): void
    {
        $expectedAmountOfApples = 3;

        $this->basket->setProducts($this->getFixtureProducts());
        $groupedProducts = $this->basket->getGroupedProducts();

        foreach ($groupedProducts as $group) {
            $this->assertTrue(is_array($group));
        }

        $this->assertSame($expectedAmountOfApples, count($groupedProducts[self::SKU_APPLE]));
    }

    /**
     * @param string $skus
     * @param array $apiResponse
     * @param int $expectedTotalPrice
     *
     * @dataProvider getBaskets
     */
    public function testBasketGetTotalPriceReturnsExpectedAmount(string $skus, array $apiResponse, int $expectedTotalPrice): void
    {
        $this->basket->setProducts($apiResponse);
        $totalPrice = $this->basket->getTotalPrice();

        $this->assertSame($expectedTotalPrice, $totalPrice->getAmount());
    }

    /**
     * Data provider for variations of basket and total price expected.
     *
     * @return array
     */
    public function getBaskets(): array
    {
        $data = $this->getFakeApiResponse();
        
        // SKUs in basket -> API response -> expected total price.
        return [
            ['', [], 0],
            ['A', [$data['A']], 50],
            ['AB', [$data['A'], $data['B']],  80],
            ['AA', [$data['A'], $data['A']], 100],
            ['CDBA', [$data['C'], $data['D'], $data['B'], $data['A']], 115],
            ['AAA', [$data['A'], $data['A'], $data['A']], 130],
            ['AAABB', [$data['A'], $data['A'], $data['A'], $data['B'], $data['B']], 175],
        ];
    }

    /**
     * @return array
     */
    private function getFixtureProducts(): array
    {
        return [
            [
                'sku' => 'A',
                'name' => 'Apple',
                'price' => 50,
                'currency' => 'GBP',
                'special_offer' => [
                    'condition_amount' => 3,
                    'offer_price' => 130
                ]
            ],
            [
                'sku' => 'A',
                'name' => 'Apple',
                'price' => 50,
                'currency' => 'GBP',
                'special_offer' => [
                    'condition_amount' => 3,
                    'offer_price' => 130
                ]
            ],
            [
                'sku' => 'A',
                'name' => 'Apple',
                'price' => 50,
                'currency' => 'GBP',
                'special_offer' => [
                    'condition_amount' => 3,
                    'offer_price' => 130
                ]
            ],
            [
                'sku' => 'B',
                'name' => 'Banana',
                'price' => 30,
                'currency' => 'GBP',
                'special_offer' => [
                    'condition_amount' => 2,
                    'offer_price' => 45
                ]
            ],
        ];
    }

    /**
     * @return array
     */
    private function getFakeApiResponse(): array
    {
        return [
            'A' => [
                'sku' => 'A',
                'name' => 'Apple',
                'price' => 50,
                'currency' => 'GBP',
                'special_offer' => [
                    'condition_amount' => 3,
                    'offer_price' => 130
                ]
            ],
            'B' => [
                'sku' => 'B',
                'name' => 'Banana',
                'price' => 30,
                'currency' => 'GBP',
                'special_offer' => [
                    'condition_amount' => 2,
                    'offer_price' => 45
                ]
            ],
            'C' => [
                'sku' => 'C',
                'name' => 'Cucumber',
                'price' => 20,
                'currency' => 'GBP',
                'special_offer' => []
            ],
            'D' => [
                'sku' => 'D',
                'name' => 'Damson',
                'price' => 15,
                'currency' => 'GBP',
                'special_offer' => []
            ],
        ];
    }
}
